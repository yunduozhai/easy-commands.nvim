local function ReplacePattern(str, pattern, replacement)
  return string.gsub(str, pattern, replacement)
end

local function trim(input)
  return input:gsub("^%s*(.-)%s*$", "%1")
end

local function contains(str, char)
  return str:find(char, 1, true) ~= nil
end

local function EndsWithSuffix(str, suffix)
  local len = #suffix
  return str:sub(-len) == suffix
end

local function splitIntoLines(i)
  local lines = {}
  for line in i:gmatch("([^\n]*)\n?") do
    if line ~= "" then
      table.insert(lines, line)
    end
  end
  return lines
end

local function join(stringList, delimiter)
  local str = ""
  for i, s in ipairs(stringList) do
    str = str .. s
    if i ~= #stringList then
      str = str .. delimiter
    end
  end
  return str
end

local function splitCmdString(cmd)
  local t = {}
  local inQuotes = false
  local currentQuoteChar = nil
  local currentWord = ""

  for i = 1, #cmd do
    local c = cmd:sub(i, i)
    if c == " " and not inQuotes then
      if #currentWord > 0 then
        table.insert(t, currentWord)
        currentWord = ""
      end
    elseif c == "'" or c == '"' then
      if inQuotes and currentQuoteChar == c then
        inQuotes = false
        currentQuoteChar = nil
      elseif not inQuotes then
        inQuotes = true
        currentQuoteChar = c
      else
        currentWord = currentWord .. c
      end
    else
      currentWord = currentWord .. c
    end
  end
  if #currentWord > 0 then
    table.insert(t, currentWord)
  end
  return t
end

local M = {
  replace_pattern = ReplacePattern,
  trim = trim,
  contains = contains,
  ends_with_suffix = EndsWithSuffix,
  split_into_lines = splitIntoLines,
  join = join,
  split_cmd_string = splitCmdString,
}

return M
